import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Form } from './components/Form/Form';

class App extends Component {
  buildLocationRows = locations => {
    if (!locations) return null;

    return Object.entries(locations).map(([key, { name, region }]) => (
      <tr key={key}>
        <td>{name}</td>
        <td>{region}</td>
      </tr>
    ));
  };

  onFormSubmit = ({ name, region }) => {
    this.props.addLocation({ name, region });
  };

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Elminda</h2>
        </div>

        <Form onSubmit={this.onFormSubmit} />

        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Region</th>
            </tr>
          </thead>
          <tbody>{this.buildLocationRows(this.props.locations)}</tbody>
        </table>
      </div>
    );
  }
}

export default App;
