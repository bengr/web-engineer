import React from 'react';

import './Form.css';

const getInitialState = () => ({ name: '', region: '' });

export class Form extends React.Component {
  state = getInitialState();

  onSubmit = event => {
    event.preventDefault();

    this.props.onSubmit({ ...this.state });

    this.state = getInitialState({});
  };

  updateInput = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  render() {
    return (
      <form className="form" onSubmit={this.onSubmit}>
        <div className="form__inputs">
          <label htmlFor="name">Name:</label>
          <input
            name="name"
            className="form__input"
            value={this.state.name}
            onChange={this.updateInput}
          />
          <label htmlFor="region">Region:</label>
          <input
            name="region"
            className="form__input"
            value={this.state.region}
            onChange={this.updateInput}
          />
        </div>

        <button id="add_location" type="submit">
          Add Name and Region
        </button>
      </form>
    );
  }
}
