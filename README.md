Elminda developed this repository as skills indicator for candidates for Front End Web Engineers. The code was scaffolded with react-create-app https://github.com/facebookincubator/create-react-app. Firebase is used as the data store via the firebase SDK https://github.com/firebase/firebase-js-sdk.

Instructions:
1. Fork the repository https://bitbucket.org/elminda/web-engineer to your own account (create an account if you don't have one).
2. Clone your new forked repository to the testing computer in the ~/Projects directory
3. Install the code via `npm install` or `yarn`
4. Start the server via `yarn start` or `npm start`
5. Write code to make unit tests pass
6. Commit your work to your forked respository
7. Tell Noga or Yehoshua the URL of your forked repository

You will have a hour to complete the test.

Contact Noga Gal at x153 or Yehoshua Jacobson at x165 with any questions.
